package com.team2052.lib.trajectory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.team2052.lib.trajectory.WaypointSequence.Waypoint;
import com.team2052.lib.trajectory.io.TextFileSerializer;

public class Main {
	public static String joinPath(String path1, String path2) {
		File file1 = new File(path1);
		File file2 = new File(file1, path2);
		return file2.getPath();
	}

	public static void writePath(Path path, String directory, String path_name) {
		// Outputs to the directory supplied as the first argument.
		TextFileSerializer js = new TextFileSerializer();
		String serialized = js.serialize(path);
		String fullpath = joinPath(directory, path_name + ".txt");
		if (!writeFile(fullpath, serialized)) {
			System.err.println(fullpath + " could not be written!!!!1");
			System.exit(1);
		} else {
			System.out.println("Wrote " + fullpath);
		}
	}

	private static boolean writeFile(String path, String data) {
		try {
			File file = new File(path);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(data);
			bw.close();
		} catch (IOException e) {
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		String directory = "paths";
		if (args.length >= 1) {
			directory = args[0];
		}

		TrajectoryGenerator.Config config = new TrajectoryGenerator.Config();
		config.dt = 1.0 / 100.0;

		final double kWheelbaseWidth = 24.375;
		{
			final String path_name = "TestPath";
			config.max_acc = 107.0;
			config.max_jerk = 600.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(5);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(5 * 12, 5 * 12, 0));

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}
		{
			final String path_name = "LowBarHighGoalPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(13 * 12, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(17 * 12, 1 * 12, Math.PI / 6));
			p.addWaypoint(new WaypointSequence.Waypoint(20.5 * 12, 4.5 * 12, Math.PI / 3));

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}

		{
			final String path_name = "Position2ToCenterPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(61, 5 * 12, Math.PI / 7));

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}
		
		{
			final String path_name = "Position2bToCenterPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(4.49 * 12, 0, Math.PI / 7));

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}

		{
			final String path_name = "Position3ToCenterPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(55, 0, Math.PI / 7));

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}

		{
			final String path_name = "Position4ToCenterPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(50, 0, 0));

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}

		{
			final String path_name = "Position5ToCenterPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(56, 54, Math.toRadians(15)));

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}

		{
			final String path_name = "Position5bToCenterPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(38, 12, Math.PI / 6));

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}
		
		{
			final String path_name = "2BallCrossPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(24, 36, 0));
			p.addWaypoint(new Waypoint(7 * 12, 36, 0));
			p.addWaypoint(new Waypoint(8 * 12, 12, Math.toRadians(30)));
			
			

			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}
		
		{
			final String path_name = "LowBarCrossBackPath";
			config.max_acc = 107.0;
			config.max_jerk = 400.0;
			config.max_vel = 4 * 12;

			WaypointSequence p = new WaypointSequence(10);
			p.addWaypoint(new WaypointSequence.Waypoint(0, 0, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(4 * 12, 4 * 12, 0));
			p.addWaypoint(new WaypointSequence.Waypoint(12 * 12, 4 * 12, 0));
			
			Path path = PathGenerator.makePath(p, config, kWheelbaseWidth, path_name);
			writePath(path, directory, path_name);
		}
	}
}
